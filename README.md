# UAS-Coding-Challenge-2020
Line Intersection Challenge (B-LNE-0)

## Getting Started
Download the repo and run:
```
cd /name_of_repo/src
```
In order to execute the program, run:
```
python tester.py
```
Manual test case input is available. You may input any case which you want evaluated into `cases.json`.
Test results are stored as images in `./test_results`. A gallery of all results is available. Run `open collage.html`, or otherwise open the included `collage.html` file to view the gallery. It should open automatically when you run the python script.

### What's Included
The solution included in this repo provides an a path which avoid the given circle obstacle.

### Known Bugs
A straight vertical line may or may not break the program, because I did not take care of this edge case :)