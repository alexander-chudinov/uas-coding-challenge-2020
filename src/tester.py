import solver, drawer
from random import randint
import webbrowser, os
import json

#it is what it is named
def run_2d_test(p0: tuple, p1: tuple, c: tuple, r:float, test_id: int):
    results = solver.solve_2d(p0,p1,c,r)
    drawer.draw_2d_test(p0,p1,c,r,test_id,results)

test_id = 0

#run manually specified cases, no error checking like a boss
with open('cases.json') as jsonText:
    jsonData = json.load(jsonText)
    for case in jsonData["cases"]:
        run_2d_test(case["p0"], case["p1"], case["c"], case["r"], test_id)
        test_id+=1

#execute random cases just because we can
for i in range(100):
    run_2d_test((randint(100,400),randint(100,400)),(randint(100,400),randint(100,400)),(randint(100,400),randint(100,400)),randint(60,100),test_id)
    test_id+=1

#open the web gallery, just a nice thing to have because we are apes with eyes and not computers
webbrowser.open_new_tab('file:///'+os.getcwd()+'/collage.html')