from PIL import Image, ImageDraw, ImageFont
import time, os

#font definitions
font = ImageFont.truetype("./assets/fonts/Quicksand-Bold.otf", 25)
font2 = ImageFont.truetype("./assets/fonts/Quicksand-Bold.otf", 20)

#does all the work
def draw_2d_test(p0: tuple, p1: tuple, c: tuple, r:float, test_id: int, results: tuple):
    img = Image.new("RGB", (500, 700))
    imgC = ImageDraw.Draw(img)

    #draw the problem
    imgC.ellipse([(c[0]-r,500-c[1]-r),(c[0]+r,500-c[1]+r)])
    imgC.line([(p0[0],500-p0[1]),(p1[0],500-p1[1])],"#FFFF00")

    #draw the bottom bar and the test id
    imgC.rectangle([(0,500),(500,700)], fill ="#f8f8f8")
    imgC.text((30, 30), "Result ID: "+str(test_id), "#ffffff", font2)

    if(results!=None):
        if(len(results)>0):
            #line height at which results start
            line = 530
            if(len(results[0])>0):
                if(type(results[0][0])!=tuple):
                    line+=30
                    imgC.text((30, line), "x:"+str(round(results[0][0],2))+", y:"+str(round(results[0][1],2)), "#000000", font)
                    imgC.text((30, 530), "Intersections Found: 1", "#000000", font)
                    imgC.ellipse([(results[0][0]-4,500-results[0][1]-4),(results[0][0]+4,500-results[0][1]+4)],"#00FF00")
                else:
                    imgC.text((30, 530), "Intersections Found: "+str(len(results[0])), "#000000", font)
                    #draw intersections in yellow
                    for result in results[0]:
                        line+=30
                        imgC.text((30, line), "x:"+str(round(result[0],2))+", y:"+str(round(result[1],2)), "#000000", font)
                        imgC.ellipse([(result[0]-4,500-result[1]-4),(result[0]+4,500-result[1]+4)],"#00FF00")
                    previousPoint = None

                    #plot the new course which avoids the obstacle
                    for c in results[3]:
                        imgC.ellipse([(c[0]-4,500-c[1]-4),(c[0]+4,500-c[1]+4)],"#0000FF")
                        if previousPoint!=None:
                            imgC.line([previousPoint,(c[0],500-c[1])],"#0000FF")
                            previousPoint = (c[0],500-c[1])
                        else:
                            previousPoint = (c[0],500-c[1])

                    #draw tangent points which were used to solve the problem in white, just for fun
                    for t in results[2]:
                        imgC.ellipse([(t[0]-4,500-t[1]-4),(t[0]+4,500-t[1]+4)],"#ffffff")
            else:
                imgC.text((30, 530), "Intersections Found: 0", "#000000", font)
        #include calculation time in the images, just for fun
        imgC.text((30, 460), "Solve Time: "+str(round(results[1]*1000,4))+" ms", "#ffffff", font2)
    img.save("./test_results/result-"+str(test_id)+".png")