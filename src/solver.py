import time, math

#2d Solver
def solve_2d(p0: tuple, p1: tuple, c: tuple, r:float) -> tuple:
    start_time = time.time()

    if(p1[0]!=p0[0]):
        #slope
        m = (p1[1]-p0[1])/(p1[0]-p0[0])
        #y-intercept
        b = p0[1]-m*p0[0]

        #some useful functions
        def f(x): return m*x+b
        def T(x): return 1/(m+1e-10)*(c[0]-x)+c[1]
        def in_segment(x): return (abs(x-p0[0])+abs(p1[0]-x))<=abs(p1[0]-p0[0])

        #discriminant
        D = (2*(m*(b-c[1])-c[0]))**2-4*(m**2+1)*(b**2+c[1]**2-2*b*c[1]-r**2+c[0]**2)

        if(D<0):
            #no intersections
            return ((),time.time()-start_time)
        elif(D==0):
            #one intersection
            #calculate x of the intersection
            x0 = (2*m*c[1]+2*c[0]-2*b*m + D**0.5)/(2*m**2+2)
            if(in_segment(x0)):
                return (((x0,f(x0))),time.time()-start_time)
            else:
                return ((),time.time()-start_time)
        else:
            #two intersections
            #calculate positions of the intersections
            x0 = (2*m*c[1]+2*c[0]-2*b*m + D**0.5)/(2*m**2+2)
            x1 = (2*m*c[1]+2*c[0]-2*b*m - D**0.5)/(2*m**2+2)

            #construct a circle which is slightly wider than the obstacle
            r0 = r+10

            #construct two tangential lines for this circle which are parallel to the straight line path
            #determine where the lines intersect the constructed circle
            xT1 = c[0]+((r0**2*m**2)/(m**2+1))**0.5
            xT2 = c[0]-((r0**2*m**2)/(m**2+1))**0.5
            xI1 = (c[0]-m*b+c[1]*m)/(m**2+1)

            #determine which tangent line is closer to the path line and select it for use in course plotting
            D1 = math.hypot((xT1-xI1),(T(xT1)-T(xI1)))
            D2 = math.hypot((xT2-xI1),(T(xT2)-T(xI1)))
            xT = xT2
            if D2>D1:
                xT = xT1

            #function of the chosen tangent
            def TL(x): return (m*x+(T(xT)-m*xT))

            #two more points are required to contruct the tangential line segment
            #a chosen length r/2 is used for each half of the segment so that there is minimal interseciton with the actual obstacle
            #the x component of this segment is calculated below
            l = r/2
            xC = (l**2/(m**2+1))**0.5

            #the two points are recorded as tuples
            cM1 = (xT-xC,TL(xT-xC))
            cM2 = (xT+xC,TL(xT+xC))

            #course points need to be put in order so that the program can draw segments
            chartedCourse = [p0,cM1,(xT,T(xT)),cM2,p1]
            #since the mid section is already in order, all that must be done is a switch of the first and last coordinates
            #hence distances between the first point and the second as well as distance between the first and second last point are compared
            if(math.hypot((p0[0]-cM1[0]),(p0[1]-cM1[1]))>math.hypot((p0[0]-cM2[0]),(p0[1]-cM2[1]))):
                chartedCourse = [p1,cM1,(xT,T(xT)),cM2,p0]

            #we must also check if potential intersections are actually part of the line segment with a domain check
            if(in_segment(x0) and in_segment(x1)):
                return (((x0,f(x0)),(x1,f(x1))),time.time()-start_time, ((xT1,T(xT1)),(xT2,T(xT2))),chartedCourse)
            elif(in_segment(x0)):
                return (((x0,f(x0))),time.time()-start_time, ((xT1,T(xT1)),(xT2,T(xT2))),(xT,T(xT)),chartedCourse)
            elif(in_segment(x1)):
                return (((x1,f(x1))),time.time()-start_time, ((xT1,T(xT1)),(xT2,T(xT2))),(xT,T(xT)),chartedCourse)
            else:
                return ((),time.time()-start_time, ())

    #heres a fossil for you, archaeologist
    #I did not have enough time to code up the solution for an edge case where the line has an infinite slope
    #this whole thing was done in 23 hours overnight

    # else:
    #     #vertical line
    #     a = p1[0]
    #     if(r<abs(a-c[0])):
    #         return ()
        # elif(in_segment):
        #     return (())